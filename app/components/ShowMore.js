import { useState } from "react";
import {
  AiOutlineUser,
  AiTwotoneCalendar,
  AiOutlineComment,
} from "react-icons/ai";
const ShowMore = ({ post, options }) => {
  const text = post.body;
  const [showMore, setShowMore] = useState(false);

  const ToggelHandeler = () => {
    setShowMore(!showMore);
  };
  return (
    <>
      {showMore ? text : text.slice(0, 200)}
      <div className="flex justify-between items-center ">
        <span className="text-xs flex items-center">
          <AiTwotoneCalendar size={20} className="pl-1" />
          {new Date(post.createdAt).toLocaleDateString("fa-IR", options)}
        </span>

        <span className="text-xs flex items-center">
          <AiOutlineComment size={20} className="pl-1" />
          نظر: {post.commentCount}
        </span>

        <span className="text-xs flex items-center">
          <AiOutlineUser size={20} className="pl-1" />
          نویسنده: {post.author}{" "}
        </span>

        <button
          onClick={ToggelHandeler}
          className="bg-primary text-white  font-bold uppercase w-28 h-8  text-xs   rounded-2xl hover:bg-transparent
          hover:text-primary hover:border-2 hover:border-primary  outline-none
            focus:outline-none  ease-linear transition-all duration-150"
        >
          {showMore ? "کمتر" : "بیشتر"}
        </button>
      </div>
    </>
  );
};

export default ShowMore;
