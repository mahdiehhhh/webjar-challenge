import React from "react";
import { AiOutlineSearch } from "react-icons/ai";
const Search = ({ searchHandler, searchTerm }) => {
  return (
    <div className="mb-14 max-w-2xl m-auto">
      <h1 className="text-3xl text-center mb-16">وبلاگ</h1>
      <label
        htmlFor="email"
        className="relative text-gray-400  block search__shadow  rounded p-2 search__shadow focus-within:text-primary"
      >
        <AiOutlineSearch className="pointer-events-none w-8 h-8 absolute top-1/2 transform -translate-y-1/2 left-3" />

        <input
          type="email"
          name="email"
          id="email"
          value={searchTerm}
          placeholder="جستوجو کنید"
          className="form-input w-full outline-none focus:text-[#7B7B7B] "
          onChange={searchHandler}
        />
      </label>
    </div>
  );
};

export default Search;
