import { AiOutlineClose } from "react-icons/ai";
import LoginForm from "./LoginForm";
const Modal = ({ showModal, setShowModal }) => {
  return (
    <>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ease-in duration-300">
            <div className="relative w-[800px]">
              {/*content*/}
              <div className="border-0  shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none ease-in duration-300">
                {/*header*/}
                <div className="p-3">
                  <button
                    className=" rounded-full shadow-md p-2"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    <span>
                      <AiOutlineClose size={20} className="text-[#6F6F6F]" />
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-16 m-auto">
                  <LoginForm setShowModal={setShowModal} />
                </div>
                {/*footer*/}
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default Modal;
