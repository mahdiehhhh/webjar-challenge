const Categories = ({ allCategories, getCategoryId }) => {
  return (
    <>
      <div className="category__shadow px-3 py-2 rounded mb-5">
        <h3>دسته بندی</h3>

        {allCategories.map((item) => (
          <div className="p-1" key={item._id}>
            <div className="flex items-center justify-between flex-row-reverse">
              <input
                type="checkbox"
                id={item._id}
                name="vehicle1"
                value={item.name}
                onChange={(e) => getCategoryId(item._id, e)}
                className="opacity-0 absolute w-4 h-4"
              />
              <div className="bg-white border-2 rounded-sm border-primary w-4 h-4 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-primary">
                <svg
                  className="fill-current hidden w-3 h-3 text-primary pointer-events-none"
                  viewBox="0 0 17 12"
                >
                  <g fill="none" fillRule="evenodd">
                    <g
                      transform="translate(-9 -11)"
                      fill="#2fd273"
                      fillRule="#2fd273"
                    >
                      <path d="m25.576 11.414c0.56558 0.55188 0.56558 1.4439 0 1.9961l-9.404 9.176c-0.28213 0.27529-0.65247 0.41385-1.0228 0.41385-0.37034 0-0.74068-0.13855-1.0228-0.41385l-4.7019-4.588c-0.56584-0.55188-0.56584-1.4442 0-1.9961 0.56558-0.55214 1.4798-0.55214 2.0456 0l3.679 3.5899 8.3812-8.1779c0.56558-0.55214 1.4798-0.55214 2.0456 0z" />
                    </g>
                  </g>
                </svg>
              </div>
              <label htmlFor={item._id} className="select-none">
                {item.name}
              </label>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default Categories;
