import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";
import Input from "../common/Input";
import http from "../services/HttpServices";

const LoginForm = ({ setShowModal }) => {
  const initialValues = {
    userName: "",

    password: "",
  };

  const [loading, setLoading] = useState(false);

  const onSubmit = async (value) => {
    setLoading(true);
    try {
      await http.post("/auth/login", value);
      setLoading(false);
      setShowModal(false);
    } catch (error) {
      console.log(error);
    }
  };

  const validationSchema = Yup.object({
    userName: Yup.string().required("فیلد نام کاربری خالی است !"),

    password: Yup.string().required("فیلد رمز عبور خالی است !"),
  });

  const formik = useFormik({ initialValues, onSubmit, validationSchema });
  return (
    <>
      <h2 className="text-center mb-7">ورود به حساب کاربری</h2>
      <form
        className="form__style w-96 table m-auto"
        onSubmit={formik.handleSubmit}
      >
        <Input
          formik={formik}
          name="userName"
          label="نام کاربری"
          placeholder="نام کاربری خود را وارد کنید "
        />

        <Input
          formik={formik}
          name="password"
          label="رمز عبور"
          type="password"
          placeholder="رمز عبور خود را وارد کنید "
        />
        <button
          type="submit"
          disabled={!formik.isValid}
          className="bg-primary text-white  font-bold uppercase w-60 h-10 table mx-auto mt-9 rounded-2xl hover:bg-transparent
                   hover:text-primary hover:border-2 hover:border-primary  outline-none
                   focus:outline-none  ease-linear transition-all duration-150 cursor-pointer focus:bg-hovercolor menu__button"
        >
          {loading ? (
            <svg
            className="animate-spin h-5 w-5 mr-3 border-black border-2 rounded-full absolute"
              viewBox="0 0 24 24"
            ></svg>
          ) : (
            ""
          )}
          ورود
        </button>
      </form>
    </>
  );
};

export default LoginForm;
