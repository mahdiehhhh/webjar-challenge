import Link from "next/link";
import { useState } from "react";
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import Image from "next/image";
import Modal from "../components/Modal";
import React from "react";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const [showModal, setShowModal] = React.useState(false);
  const navHandler = () => {
    setNav(!nav);
  };

  return (
    <div className="flex w-full h-10 md:h-20 shadow-md  shadow-[#f3f3f3] z-[100]">
      <div className="hidden justify-around items-center w-full h-full md:flex">
        <div className="align-middle hidden md:flex h-full ">
          <div className="items-center flex">
            <Image
              src="/../public/assets/images/logo.png"
              alt="logo"
              width={39}
              height={35}
            />
          </div>
          <ul className="pr-40 flex">
            <Link href="#" className="li__nav h-full flex items-center">
              <li>خانه</li>
            </Link>

            <Link href="#" className="li__nav  h-full flex items-center">
              <li>محصولات</li>
            </Link>

            <Link href="#" className="li__nav h-full flex items-center">
              <li>خدمات</li>
            </Link>

            <Link href="/" className="li__nav active h-full flex items-center">
              <li>وبلاگ</li>
            </Link>
          </ul>
        </div>

        <button
          onClick={() => setShowModal(true)}
          className="bg-transparent hover:border-pri
           text-primary transition ease-in-out delay-100
            border-solid border border-primary  duration-300  rounded-3xl w-48 h-10 text-lg
            menu__button active:shadow-none disabled:bg-disablecolor"
        >
          ورود
        </button>
        <Modal showModal={showModal} setShowModal={setShowModal} />
      </div>

      <div
        onClick={navHandler}
        className="flex items-center px-3 w-full justify-between md:hidden"
      >
        <div>
          <Image
            src="/../public/assets/images/logo.png"
            alt="logo"
            width={39}
            height={35}
          />
        </div>
        <AiOutlineMenu size={20} className=" text-[#6F6F6F]" />
      </div>

      <div
        className={
          nav
            ? "md:hidden fixed left-0 top-0 bg-black/60 w-full h-screen ease-in duration-800 z-10"
            : "ease-in duration-800 w-[-100%]"
        }
      >
        <div
          className={
            nav
              ? "md:hidden fixed left-0 top-0 w-52 bg-white h-screen p-3 ease-in duration-500"
              : "fixed left-[-100%] h-screen ease-in duration-300"
          }
        >
          <div onClick={navHandler} className="flex w-full justify-end">
            <span className="rounded-full shadow-md p-2">
              <AiOutlineClose size={20} className="text-[#6F6F6F]" />
            </span>
          </div>

          <div className="flex flex-col justify-around h-96">
            <ul className="mt-10">
              <Link href="#" className="li__nav">
                <li>خانه</li>
              </Link>

              <Link href="#" className="li__nav">
                <li>محصولات</li>
              </Link>

              <Link href="#" className="li__nav">
                <li>خدمات</li>
              </Link>

              <Link href="/" className="li__nav">
                <li>وبلاگ</li>
              </Link>
            </ul>
            <button
              className="bg-transparent hover:bg-hovercolor
           text-primary transition ease-in-out delay-100
            hover:text-white border border-primary 
            hover:border-transparent duration-300  rounded-3xl w-40 h-10 text-lg"
            >
              ورود
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
