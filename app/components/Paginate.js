import ReactPaginate from "react-paginate";

const Paginate = ({ pageCount, handlePageClick }) => {
  return (
    <ReactPaginate
      className="flex justify-center flex-row-reverse items-center mt-10"
      breakLabel="..."
      nextLabel="<"
      onPageChange={handlePageClick}
      pageRangeDisplayed={5}
      pageCount={pageCount}
      previousLabel=">"
      renderOnZeroPageCount={null}
      pageClassName="paginate-link"
      activeClassName="active-paginate"
      breakClassName="break-link"
      previousClassName="arrow-link"
      nextClassName="arrow-link"
    />
  );
};

export default Paginate;
