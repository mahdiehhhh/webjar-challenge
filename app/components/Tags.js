import { AiOutlineLeft } from "react-icons/ai";
const Tags = () => {
  return (
    <div className="flex items-center mb-16">
      <span className="text-sm">خانه</span>
      <AiOutlineLeft />
      <span className="text-sm active__tag">وبلاگ</span>
    </div>
  );
};

export default Tags;
