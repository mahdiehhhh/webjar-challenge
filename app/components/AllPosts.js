import http from "../services/HttpServices";
import { useEffect, useState, useCallback } from "react";
import Search from "./Search";

import Post from "./Post";
import Categories from "./Categories";
import Paginate from "./Paginate";

const AllPost = () => {
  const [allPosts, setAllPost] = useState([]);

  const [allCategories, setAllCategories] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);

  const [searchPost, setSearchPost] = useState([]);

  const [searchTerm, setsearchTerm] = useState("");

  const [PostCout, setPostCount] = useState(0);

  const [categoryId, setcategoryId] = useState([]);

  // const [filterData, setFilterData] = useState([]);

  let options = { year: "numeric", month: "long", day: "numeric" };

  const itemPerPage = 4;

  const pageCount = Math.ceil(PostCout / itemPerPage);

  const getPost = useCallback(() => {
    http
      .get(
        `/posts?limit=${itemPerPage}&skip=${(currentPage - 1) * itemPerPage}`
      )
      .then((res) => {
        setAllPost(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    getPostCout();
  }, [currentPage]);

  const getAllData = () => {
    http.get("/posts").then((res) => {
      setSearchPost(res.data);
    });
  };

  useEffect(() => {
    if (categoryId.length === 0) {
      getPost();
    }
    getAllData();
    http.get("/posts/count").then((resp) => {});

    http.get("/post-categories").then((resp) => {
      setAllCategories(resp.data);
    });

    http.get("/post-categories/count").then((resp) => {});
  }, [categoryId.length, getPost]);

  const filterCategoryData = () => {
    if (categoryId.length === 0) {
      http.get("/posts").then((res) => {
        setAllPost(res.data);
      });
    } else {
      categoryId.map((item) => {
        http.get(`/posts?category=${item}`).then((res) => {
          setAllPost(res.data);
        });
      });
    }
  };
  const getPostCout = () => {
    http.get("/posts/count").then((res) => {
      setPostCount(res.data);
    });
  };

  const searchHandler = (e) => {
    const event = e.target.value;

    if (event !== " ") {
      setsearchTerm(event);
      const filterContacts = searchPost.filter((c) => {
        return Object.values(c)
          .join(" ")
          .toLowerCase()
          .includes(event.toLowerCase());
      });
      setAllPost(filterContacts);
    } else {
      setAllPost(allPosts);
    }
  };

  const handlePageClick = (event) => {
    window.scrollTo({ top: 30, behavior: "smooth" });

    setCurrentPage(event.selected + 1);
  };

  const getCategoryId = (id, e) => {
    if (e.target.checked) {
      setcategoryId([...categoryId, id]);
      filterCategoryData();
    } else {
      setcategoryId(categoryId.filter((item) => item !== id));
    }
  };

  return (
    <>
      <Search searchHandler={searchHandler} searchTerm={searchTerm} />

      <div className="grid md:grid-cols-4 sm:grid-cols-1 gap-4">
        <div className="md:col-span-1">
          <Categories
            allCategories={allCategories}
            getCategoryId={getCategoryId}
          />
        </div>
        <div className="md:col-span-3">
          <Post allPosts={allPosts} options={options} />

          <Paginate pageCount={pageCount} handlePageClick={handlePageClick} />
        </div>
      </div>
    </>
  );
};

export default AllPost;
