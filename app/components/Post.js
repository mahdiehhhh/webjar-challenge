import { useState } from "react";
import Image from "next/image";
import ShowMore from "./ShowMore";

const Post = ({ allPosts, options }) => {
  return (
    <>
      {allPosts.map((post) => (
        <div
          key={post._id}
          className="md:flex md:flex-row sm:flex-col shadow__Style mb-5 rounded-lg"
        >
          <Image
            src="/../public/assets/images/image.png"
            width={200}
            height={200}
            alt={post.title}
          />
          <div className="p-4 flex flex-col justify-between">
            <h3>{post.title}</h3>
            <ShowMore post={post} options={options} />
          </div>
        </div>
      ))}
    </>
  );
};

export default Post;
