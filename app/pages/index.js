import Head from "next/head";
import { Inter } from "@next/font/google";
import Layout from "../Layout/layout";
import AllPost from "../components/AllPosts";
import Tags from "../components/Tags";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <div lang="fa" dir="RTL">
      <Head>
        <title>Web Jar</title>
        <meta name="description" content="create by MahdiehAkbari" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <div className="max-w-6xl mx-auto px-4 sm:px-6 py-8">
          <Tags />
          <AllPost />
        </div>
      </Layout>
    </div>
  );
}
