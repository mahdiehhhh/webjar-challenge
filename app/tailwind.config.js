/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  // corePlugins: {
  //   preflight: false,
  // },
  theme: {
    container: (theme) => ({
      padding: {
        DEFAULT: theme("spacing.4"),
        sm: theme("spacing.5"),
        lg: theme("spacing.6"),
        xl: theme("spacing.8"),
      },
    }),
    extend: {
      fontFamily: {
        Iranyekan: ["Iranyekan"],
      },
      colors: {
        primary: "#00C853",
        secondary: "#FE5656",
        hovercolor: "#7ee1a6",
        bordercolor: "#00712F",
        disablecolor: "#00712F",
        loadingcolor: "#00C853",
      },
    },
  },
  plugins: [],
};
