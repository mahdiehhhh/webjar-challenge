const Input = ({ name, type = "text", formik, label, placeholder }) => {
  return (
    <>
      <div className="formControl flex flex-col mb-5 ">
        <p htmlFor={name}>{label}</p>
        <input
          className="border p-2 rounded-lg mt-2 focus:bg-white active:bg-white focus-within:bg-white focus-within:border border-black"
          id={name}
          type={type}
          name="name"
          placeholder={placeholder}
          {...formik.getFieldProps(name)}
        />

        {formik.errors[name] && formik.touched[name] && (
          <div className="error">{formik.errors[name]}</div>
        )}
      </div>
    </>
  );
};

export default Input;
